---
title: "GitLab's Functional Group Updates - May"
author: Trevor Knudsen
author_gitlab: tknudsen
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on in May"
canonical_path: "/blog/2018/05/01/functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our GitLab Team call, a different Functional Group gives an update to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->


----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/1zfv90uU7iI6HAKKARb52jUz8gbdLNJjVb5T94FJm978/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/fKmrpCsfIL0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1AdUrBgQ1RnSZoYbt3k2UItLykXo-jeXcT4lN9IvLssk/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xmIPV5pROUc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

----

### General Team

[Presentation slides](https://docs.google.com/presentation/d/15vqAFVW_PKgMiwqWHTM_g9qHonPvNrogZeelVbJSj_w/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/dOVFfgWs7rI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/product-may14)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/HErryVkQiHE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Meltano Team

[Presentation slides](https://drive.google.com/file/d/1oNiCtHkorYKq19kx8CwGr8Z7QCjVQiOj/view?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/mUKhrcerWDs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/15B_Nr8q9TAeiluXl3Yifkre9uw6LmdxOcSBhNLG5OFk/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nIAXV84HngA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Ditribution Team

[Presentation slides](https://docs.google.com/presentation/d/1JKzAzss7VWFR6W2SnPuVM2qXbXpA11yozqNqOmN8kJg/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gD8dPjh5OdY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----


### Monitoring Team

[Presentation slides](https://docs.google.com/presentation/d/1ddO2gTuTMxtJCKmf5ux_jlP6mhVTEdvn0wY4nkl4XKQ/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KSvm8qjMoLY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/1tLqPf04XvYunUbdHMbaUAyu4EIHboSP6FOfwb3VtdpI/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/-Zlpx6UD1GU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----
